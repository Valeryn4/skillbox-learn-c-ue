#pragma once

#include <string>

class PlayerScore {
	std::string _name = "";
	int _score = 0;

public :
	PlayerScore() {

	}


	PlayerScore(std::string name, int score) :
		_name(name), _score(score) {

	}

	PlayerScore(const PlayerScore& copy) :
		_name(copy._name), _score(copy._score) {

	}

	std::string name() const { return _name; }
	int score() const { return _score; }


	PlayerScore& operator=(const PlayerScore& from) {
		_name = from._name;
		_score = from._score;
		return *this;
	}
};

class LeaderBoard {
	
	int _size = 0;
	int _capacity = 0;
	PlayerScore* _data = nullptr;

	int partition(PlayerScore* arr, int l, int h, const PlayerScore &pivot) {
		int idx = l;

		for (int i = l; i <= h; i++) {

			if (arr[i].score() >= pivot.score()) {
				std::swap(arr[idx], arr[i]);
				idx++;
			}
		}

		idx--;

		return idx;
	}

	void quick_sort(PlayerScore* arr, int l, int h) {
		if (l < h) {
			PlayerScore pivot = arr[h];
			int idx = partition(arr, l, h, pivot);
			quick_sort(arr, l, idx - 1);
			quick_sort(arr, idx + 1, h);
		}
	}

public :
	LeaderBoard() : LeaderBoard(1)
	{
	}

	LeaderBoard(int capacity) {
		if (capacity <= 0)
			throw std::exception("LeaderBoard init failed! capacity <= 0!");
		_capacity = capacity;
		_size = 0;
		_data = new PlayerScore[capacity];
	}

	~LeaderBoard() {
		if (_data) {
			delete[] _data;
		}
	}

	void add_player(const PlayerScore& player) {
		if (_size == _capacity) {
			int new_capacity = _capacity * 1.5 + 1;
			_capacity = new_capacity;

			PlayerScore* new_data = new PlayerScore[new_capacity];
			std::memcpy(new_data, _data, sizeof(PlayerScore) * _size);

			delete[] _data;
			_data = new_data;
		} 

		_data[_size] = player;
		_size += 1;
	}

	const PlayerScore& get_player(int idx) const {
		if (idx < _size) {
			return _data[idx];
		}

		throw std::exception("get player - failed! idx >= current size leader board!");
	}

	int size() const {
		return _size;
	}

	void clear() {
		delete[] _data;
		_size = 0;
		_capacity = 0;
	}

	bool empty() const {
		return _size == 0;
	}

	std::string to_string() const {
		std::string str = "[";

		for (int i = 0; i < _size; ++i) {
			PlayerScore player = _data[i];
			std::string element = player.name();
			element += ":" + std::to_string(player.score());
			str += element + (i < _size - 1 ? ", " : "");

		}

		return str + "]";
	}

	void sort() {
		quick_sort(_data, 0, _size - 1);
	}
};