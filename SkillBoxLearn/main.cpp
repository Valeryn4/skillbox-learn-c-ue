
#include "Helpers.hpp"
#include "FindOddNumbers.hpp"
#include "Vector.hpp"
#include "PlayerLeaderboard.hpp"
#include "Animal.hpp"

#include <random>
#include <string>
#include <iostream>
#include <time.h>

static void learn_13_4() {
	// LEARN 13.4
	std::random_device r;
	std::default_random_engine gen(r());
	std::uniform_real_distribution<> rand_range(0.1, 10.0);


	float res = sum_quad(
		rand_range(gen),
		rand_range(gen)
	);
}

static void learn_14_4() {
	std::string str = "ajhsdghagsdhahgsd";

	std::cout << "STRING:" << str << std::endl;
	std::cout << "FIRST:" << str.front() << std::endl;
	std::cout << "LAST :" << str.back() << std::endl;
}

static void learn_15_4() {

	int num = 0;
	bool is_odd = false;
	std::cout << "input num:";
	std::cin >> num;
	std::cout << std::endl;
	std::cout << "is odd? (Y|N):";
	std::string YN;
	std::cin >> YN;;
	std::cout << std::endl;

	char ch = std::tolower(YN.front());
	is_odd = ch == 'y' ? true : false;

	print_odd_numbers(num, is_odd);
}

static void learn_16_5() {

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	int current_day = buf.tm_mday;

	std::cout << "input matrix size:";
	int size = 0;
	std::cin >> size;
	std::cout << std::endl;

	std::cout << " MATRIX " << std::endl;
	for (int x = 0; x < size; ++x) {
		int sum = 0;
		for (int y = 0; y < size; ++y) {
			int val = x + y;
			std::cout << "[" << val << "]\t";
			sum += val;
		}
		if (current_day % size == x)
			std::cout << " SUM:" << sum;
		std::cout << std::endl;
	}
}

static void learn_17_5() {
	Vector v1(10, 20, 30);
	Vector v2(5, 4, 1);

	std::cout << " Vector 1:        " << v1.to_string() << std::endl;
	std::cout << " Vector 2:        " << v2.to_string() << std::endl;
	std::cout << " Vector 1 length: " << v1.length() << std::endl;
	std::cout << " Vector 2 length: " << v2.length() << std::endl;
	std::cout << " Vector 1 distance to Vector 2:  " << v1.distance_to(v2) << std::endl;

}

static void learn_18_5() {
	int count_player = 0;
	std::cout << " input count player in leader board:";
	std::cin >> count_player;
	std::cout << std::endl;

	LeaderBoard leader_board(count_player);

	while (count_player != 0) {
		std::string name;
		std::cout << "NAME :";
		std::cin >> name;

		int score = 0;
		std::cout << "SCORE:";
		std::cin >> score;
		std::cout << std::endl;

		PlayerScore player(name, score);
		leader_board.add_player(player);

		count_player--;
	}

	std::cout << "LEADER BOARD (no sort):\n" << leader_board.to_string() << std::endl;
	std::cout << "sorting..." << std::endl;
	leader_board.sort();
	std::cout << "LEADER BOARD (sort)   :\n" << leader_board.to_string() << std::endl;
}

static void learn_19_5() {
	Animal* pool[] = {
		new Cat(),
		new Dog(),
		new Dog(),
		new Cat(),
		new Cow(),
		new Dog(),
		new Cow(),
	};

	for (Animal* animal : pool) {
		animal->voice();
		delete animal;
	}
}

int main() {

	std::cout << std::endl;
	std::cout << "==========================" << std::endl;
	std::cout << "Select learn:" << std::endl;
	std::cout << "1 - learn 13.4 " << std::endl;
	std::cout << "2 - learn 14.4 " << std::endl;
	std::cout << "3 - learn 15.4 " << std::endl;
	std::cout << "4 - learn 16.5 " << std::endl;
	std::cout << "5 - learn 17.5 " << std::endl;
	std::cout << "6 - learn 18.5 " << std::endl;
	std::cout << "7 - learn 19.5 " << std::endl;



	std::cout << "0 - EXIT " << std::endl;
	std::cout << std::endl;
	bool is_exit = false;
	while (!is_exit) {


		std::cout << "==========================" << std::endl;
		std::cout << " SELECT LEVEL:";
		int num_level = -1;
		std::cin >> num_level;

		std::cout << std::endl;
		std::cout << "LEARN" << std::endl;

		switch (num_level) {

		case 1:
			learn_13_4();
			break;
		case 2:
			learn_14_4();
			break;
		case 3:
			learn_15_4();
			break;
		case 4:
			learn_16_5();
			break;
		case 5 :
			learn_17_5();
			break;
		case 6 :
			learn_18_5();
			break;
		case 7 :
			learn_19_5();
			break;

		case 0 :
			is_exit = true;
			break;
		}


	}

	//LEARN 14.4


	return 0;
}
