#pragma once

#include <iostream>
#include <string>

class Animal {

protected:

	virtual std::string _voice() const {
		return "";
	}

public :
	Animal() {}

	// � ����� ����������� ��������� �����, �.�. ������ ���������� ���������� �� ������ ������ �����������.
	// ���� ��� �� ����� ����������� ����� ����� ������
	void voice() const {
		std::cout << _voice() << std::endl;
	}
};

class Cat : public Animal {

protected:
	std::string _voice() const override {
		return "MEOW!";
	}
};

class Dog : public Animal {

protected:
	std::string _voice() const override {
		return "WOOF!";
	}
};

class Cow : public Animal {

protected:
	std::string _voice() const override {
		return "MOOO!";
	}
};