#pragma once

float sum_quad(float a, float b) {
	float sum = a + b;
	return sum * sum;
}