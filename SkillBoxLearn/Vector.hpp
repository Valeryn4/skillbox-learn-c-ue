#pragma once
#include <cmath>
#include <string>

class Vector {

	float _x;
	float _y;
	float _z;

public :
	Vector(float x, float y, float z) :
		_x(x), _y(y), _z(z) {

	}

	Vector(const Vector& copy) :
		_x(copy._x), _y(copy._y), _z(copy._z) {

	}

	Vector(Vector&& move) :
		_x(move._x), _y(move._y), _z(move._z) {

	}

	float x() const { return _x; }
	float y() const { return _y; }
	float z() const { return _z; }

	Vector copy() { return Vector(*this); }

	float length() const {
		return std::sqrt(_x * _x + _y * _y + _z * _z);
	}

	Vector abs() {
		return Vector(std::abs(_x), std::abs(_y), std::abs(_z));
	}

	float distance_to(const Vector& to) {
		return (*this - to).abs().length();
	}

	std::string to_string() {
		return "[" + std::to_string(_x) + "," + std::to_string(_y) + "," + std::to_string(_z) + "]";
	}


	Vector operator-(const Vector& from) {
		return Vector(
			_x - from._x,
			_y - from._y,
			_z - from._z
		);
	}

};